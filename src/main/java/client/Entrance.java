package client;


import org.json.JSONObject;

import java.io.*;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static utils.Constants.SERVER_PORT;
import static utils.Constants.SERVER_URL;

public class Entrance {

    public static boolean register(JSONObject userData) throws IOException {

        Socket socket = new Socket(SERVER_URL, SERVER_PORT);
        ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream reader = new ObjectInputStream(socket.getInputStream());
        System.out.println("Connected and got streams");
        String password = userData.getString("Password");

        try {
            if (isUnique(userData.getString("Username"), writer, reader)) {
                userData.remove("Password");
                userData.put("Password", hashing(password));
                writer.writeObject(userData.toString());
                writer.flush();
                System.out.println("Sent userdata to register");
                System.out.println("End");
                return true;
            }
        } catch (NoSuchAlgorithmException | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        System.out.println("End");
        return false;
    }

    private static String hashing(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[] pswd = md.digest();
        return new String(pswd);
    }

    private static boolean isUnique(String username,  ObjectOutputStream writer, ObjectInputStream reader) throws IOException, ClassNotFoundException {
        writer.writeObject(new JSONObject().put("Username", username).put("Flag", "Register").toString());
        writer.flush();
        System.out.println("Sent a username to check");
        String serverMessage = (String) reader.readObject();
        JSONObject user = new JSONObject(serverMessage);
        System.out.println(user.getBoolean("IsAccepted"));
        return  user.getBoolean("IsAccepted");
    }

    public static boolean login(JSONObject userData) throws IOException, NoSuchAlgorithmException, ClassNotFoundException {
        Socket socket = new Socket(SERVER_URL, SERVER_PORT);
        ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream reader = new ObjectInputStream(socket.getInputStream());
        System.out.println("Connected and got streams");
        return isExist(userData.getString("Username"), hashing(userData.getString("Password")), writer, reader);
    }

    private static boolean isExist(String username, String password, ObjectOutputStream writer, ObjectInputStream reader) throws IOException, ClassNotFoundException {
        writer.writeObject(new JSONObject().put("Username", username).put("Flag", "Login")
                                           .put("Password", password).toString());
        writer.flush();
        System.out.println("Sent a username and password to check");
        String serverMessage = (String) reader.readObject();
        JSONObject user = new JSONObject(serverMessage);
        System.out.println(user.getBoolean("IsAccepted"));
        return  user.getBoolean("IsAccepted");
    }

}
