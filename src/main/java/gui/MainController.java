package gui;

import client.Client;
import client.Entrance;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.json.JSONObject;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class MainController {

    @FXML
    private AnchorPane clientPane;

    @FXML
    private Button regB;

    @FXML
    private Button backB;

    @FXML
    private TextField regUsrnmF;

    @FXML
    private TextField regMailF;

    @FXML
    private TextField regPswdF;

    @FXML
    private TextField regConfirmPswdF;

    @FXML
    private TextField loginUsrnmF;

    @FXML
    private TextField loginPswdF;

    @FXML
    private TextArea ClientMessageArea;

    public static TextArea area;

    @FXML private void initialize() {

        Platform.runLater(() -> {
            if(clientPane != null) {
                area = (TextArea) clientPane.getChildren().get(0);
            }
        });
    }

    @FXML
    private void registerUser(ActionEvent event) {
        System.out.println("Start");
        String username = regUsrnmF.getText();
        String mail = regMailF.getText();
        String password = regPswdF.getText();
        String acceptPassword = regConfirmPswdF.getText();
        JSONObject userData = new JSONObject();

        if (!username.isEmpty() && !mail.isEmpty() && !password.isEmpty() &&
            !acceptPassword.isEmpty() && password.equals(acceptPassword)) {
            try {
                userData.put("Username", username).put("Mail", mail).put("Password", password);

                if (Entrance.register(userData)) {
                    regUsrnmF.setText("�� ������� ������������������");
                } else {
                    regUsrnmF.setText("������ �����������(��� ������������ ������)");
                }
            } catch (IOException e) {
                e.printStackTrace();
                regUsrnmF.setText("������ �����������(��� ���������� � ��������)");
            }
        } else {
            regUsrnmF.setText("������� ������ ���������");
        }
    }

    @FXML
    private void clickOnReg(ActionEvent event) {
        Stage stage = (Stage) regB.getScene().getWindow();
        stage.setScene(Gui.regScene);
        stage.show();
    }

    @FXML
    private void clickBack(ActionEvent event) {
        Stage stage = (Stage) backB.getScene().getWindow();
        stage.setScene(Gui.signScene);
        stage.show();
    }

    public void clickOnLogin(ActionEvent event) {
        String username = loginUsrnmF.getText();
        System.out.println("Username: " + username);
        String password = loginPswdF.getText();
        JSONObject userData = new JSONObject();

        if (!username.isEmpty()  && !password.isEmpty()) {
            try {
                userData.put("Username", username).put("Password", password);

                if (Entrance.login(userData)) {
                    Stage stage = (Stage) loginUsrnmF.getScene().getWindow();
                    stage.setScene(Gui.clientScene);
                    stage.show();
                    new Thread(() -> {
                        System.out.println("start client session");
                        try {
                            Client.startClient(username);
                        } catch (IOException | ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }).start();
                } else {
                    loginUsrnmF.setText("������ �����");
                }
            } catch (IOException | NoSuchAlgorithmException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            loginUsrnmF.setText("������� ������ ���������(���� ������)");
        }
    }


    public void send(ActionEvent event) {
        String message = Client.user + ": " + ClientMessageArea.getText();
        Client.sendMessageToServer(message);
    }

    public static void append(String text) {
        System.out.println(text);
        area.appendText(text + '\n');
    }


}
