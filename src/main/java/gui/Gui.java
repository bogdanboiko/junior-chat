package gui;


import client.Client;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Gui extends Application {

    protected static Scene signScene;
    protected static Scene regScene;
    protected static Scene clientScene;
    protected static String type;

    @Override
    public void start(Stage stage) throws Exception {
        signScene = FXMLLoader.load(getClass().getResource("/SignInWindow.fxml"));
        regScene = FXMLLoader.load(getClass().getResource("/RegistrationWindow.fxml"));
        clientScene = FXMLLoader.load(getClass().getResource("/ClientWorkspace.fxml"));
        Scene scene = signScene;
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        Client.sendMessageToServer("������������ " + Client.user + " ������� ���");
        Client.socket.close();
        super.stop();
    }
}
